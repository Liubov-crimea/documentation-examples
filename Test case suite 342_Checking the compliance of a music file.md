Test case suite 342
“Checking the compliance of a music file (track) with company regulations 1.3 “Regulations for working with a music database””

Test case 01-3.63 Name validation check:
1) control the absence of punctuation marks and symbols in the title
2) check the name format with the sample (Xxxx - Yyyy)

Test case 01-3.64 Check for missing tags:
1) Go to the properties of the music file
2) Look at the “Details” tab
3) The “Value” column should not contain information other than the allowed one*
*name, format, creation date, duration, size, bitrate

Test case 01-3.65 Checking the external technical characteristics of the track - Checking the volume:
1) Open the file in program “MP3Gain”
2) Press the button “Track Analysis”
3) Permissible result value in the “Target volume” window is 89.0-89.9 dB

Test case 01-3.66 Checking the external technical characteristics of the track - Checking the correct flow rate:
1) Add the “Stream rate” column in the folder with files
2) Values must be equal to 320 kb/s

Test case 01-3.67 Checking the external technical characteristics of the track - Checking the track for the absence of silence:
1) Open the track in program “Audacity”
2) The wave pattern must not contain a horizontal line (a graphic representation of silence).
