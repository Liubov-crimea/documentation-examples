Instruction for the user. Section “Download playlist”
1. Go to the portal page (http://content-portal.indoormedia.ua)
2. Enter your personal account (a letter with credentials is already in your mail)
3. Select the "My playlists" section.
4. Then select the desired playlist.
5. The "Listen" or "Download" options open. Click "Download".
6. The playlist is downloaded as a .zip archive (you need to wait a bit, then unzip it).
7. If you did not find a letter with credentials or the download of the archive did not start, contact the support service.
