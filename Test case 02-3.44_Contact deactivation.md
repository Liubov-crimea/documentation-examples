Test case 02-3.44 “Contact deactivation”
1. Go into the system.
2. Go to the "Contacts" section
3. In the “Search” window, enter the desired contact
4. Go to the user card
5. Select the tab “Basic Information”
6. Go to the section “Means of communication”
7. Uncheck the checkboxes of all means of communication (mail, mobile number, etc.)
8. Then uncheck "Is up-to-date"
9. Click the “Save” button
10. Press the “Exit” button
